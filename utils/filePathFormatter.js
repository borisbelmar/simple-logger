const dateformat = require('dateformat')

const filePathFormatter = (project, id) => {
  const dateString = dateformat(new Date(), 'yyyymmdd')
  const projectPath = project ? (project.replace(/[^A-Z0-9]/gi, '-') + '/') : ''
  const fileName = id ? ('.' + id.replace(/[^A-Z0-9]/gi, '-')) : ''
  return 'logs/' + projectPath + dateString + fileName + '.log'
}

module.exports = filePathFormatter