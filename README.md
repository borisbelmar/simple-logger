Simple Event Logger
---
![Version](https://img.shields.io/badge/version-1.0.1-brightgreen)
![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/borisbelmar/simple-logger/master)

Simple server logger with Koa. It store sent logs to .log file. The endpoint accepts just `text/plain` content-type for now.

The headers for tag the logs are:

| Header | Description |
| ------ | ------ |
| x-log-id | ID for log's file name append. |
| x-project-name | Folder name where logs will be stored. |

The logs are stored with the next name convention:

```bash
"<root_folder>/logs/<x-project-name>/yyyymmdd.<x-log-id>.log"

# Example
# x-log-id = 1sa7fc7
# x-project-name = awesome-project

"simple-logger/logs/awesome-project/20200808.1sa7fc7.log"

```

Using Docker or Podman
---

You can use the public docker image for run this simple log files generator using the code below:

```bash
mkdir logs
docker run --rm -d -p 3000:3000 -v $(pwd)/logs:/app/logs/ borisbelmar/simple-logger
```

Now you can start using your endpoint:

```bash
curl http://localhost:3000 -X POST -H "x-project-name: awesome-project" -H "x-log-id: 12345" -H "Content-type: text/plain" -d Testing
```

Todo list
---

- [ ] Parse array
- [ ] Unit testing
- [ ] Parse JSON
- [ ] Store logs as JSON
- [ ] Store logs as YAML
- [ ] Connect logs to MongoDB