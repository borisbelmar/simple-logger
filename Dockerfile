FROM node:12-alpine

COPY ["package.json", "yarn.lock", "/app/"]

WORKDIR /app/

RUN yarn

COPY ["index.js", "."]

COPY ["utils/", "./utils/"]

CMD ["yarn", "start"]