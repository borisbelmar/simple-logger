const fs = require('fs')
const Koa = require('koa')
const morgan = require('koa-morgan')
const bodyParser = require('koa-bodyparser')
const cors = require('@koa/cors')
const serve = require('koa-static')
const mount  = require('koa-mount')

const getFullUrl = require('./utils/getFullUrl')
const filePathFormatter = require('./utils/filePathFormatter')

const PORT = process.env.PORT || 3000

const app = new Koa()

app.use(morgan('dev'))
app.use(cors())
app.use(bodyParser({
  enableTypes: ['text']
}))

app.use(mount( '/logs', serve('./logs'))) ;

// response
app.use(ctx => {
  const id = ctx.headers['x-log-id']
  const project = ctx.headers['x-project-name']
  ctx.assert(
    Object.entries(ctx.request.body) != 0,
    400,
    'Must come text/plain content in body \n'
  )
  try {
    if (!fs.existsSync('logs')) {
      fs.mkdirSync('logs')
    }
    if (project && !fs.existsSync(`logs/${project}`)) {
      fs.mkdirSync(`logs/${project}`)
    }
    
    const file = filePathFormatter(project, id)
    fs.appendFileSync(file, ctx.request.body.replace(/\n+$/, '') + '\n')
    fs.chmodSync(file, 0666)
    ctx.status = 201
    ctx.body = getFullUrl(ctx.request) + file + '\n'
  } catch(err) {
    ctx.status = 500
    ctx.body = err.message + '\n'
  }
})

app.listen(PORT, () => console.log('🚀 Logger server on port', PORT))